from typing import IO, Optional, TextIO, BinaryIO, Union, cast, overload
from io import RawIOBase, BufferedIOBase, TextIOBase, TextIOWrapper
from pathlib import Path


AnyBinaryIO = Union[BinaryIO, BufferedIOBase, RawIOBase]
AnyTextIO = Union[TextIO, TextIOBase]


@overload
def greet(anyio: AnyTextIO, encoding: None = ...) -> None:
    ...


@overload
def greet(anyio: AnyBinaryIO, encoding: Optional[str] = ...) -> None:
    ...


def greet(anyio: Union[AnyBinaryIO, AnyTextIO], encoding: Optional[str] = None) -> None:
    textio = as_textio(anyio)
    textio.write("Hello!\n")


@overload
def as_textio(anyio: AnyTextIO, encoding: None = ...) -> IO[str]:
    ...


@overload
def as_textio(anyio: AnyBinaryIO, encoding: Optional[str] = ...) -> IO[str]:
    ...


def as_textio(
    anyio: Union[AnyBinaryIO, AnyTextIO], encoding: Optional[str] = None
) -> IO[str]:
    if hasattr(anyio, "encoding"):
        return cast(IO[str], anyio)
    return TextIOWrapper(cast(IO[bytes], anyio), encoding=encoding)


def test_greeter(tmp_path: Path) -> None:
    io_bytes: IO[bytes]
    io_str: IO[str]

    text_io: TextIO
    binary_io: BinaryIO

    with (tmp_path / "a").open("wb") as bfileh:
        binary_io = bfileh
        io_bytes = bfileh
        assert binary_io is io_bytes
        greet(bfileh)
    with (tmp_path / "a").open("wb") as bfileh:
        greet(cast(BufferedIOBase, bfileh))
    with (tmp_path / "b").open("wb", buffering=0) as rbfileh:
        binary_io = rbfileh
        io_bytes = rbfileh
        assert binary_io is io_bytes
        greet(rbfileh)
    with (tmp_path / "b").open("wb", buffering=0) as rbfileh:
        greet(cast(RawIOBase, rbfileh))
    with (tmp_path / "c").open("w") as tfileh:
        text_io = tfileh
        io_str = tfileh
        assert text_io is io_str
        greet(tfileh)
    with (tmp_path / "c").open("w") as tfileh:
        greet(cast(TextIOBase, tfileh))

