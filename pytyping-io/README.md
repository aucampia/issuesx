## output

```
$ make
poetry run pytest test_io.py test_other.py
============================================================================ test session starts ============================================================================
platform linux -- Python 3.7.12, pytest-6.2.5, py-1.10.0, pluggy-1.0.0
rootdir: /home/iwana/sw/d/gitlab.com/aucampia/issues/pytyping-io
collected 2 items

test_io.py .                                                                                                                                                          [ 50%]
test_other.py .                                                                                                                                                       [100%]

============================================================================= 2 passed in 0.01s =============================================================================
poetry run mypy --show-error-context --show-error-codes test_io.py test_other.py
test_other.py: note: In function "test_types":
test_other.py:17: error: Incompatible types in assignment (expression has type "BinaryIO", variable has type "BufferedIOBase")  [assignment]
test_other.py:22: error: Non-overlapping identity check (left operand type: "BinaryIO", right operand type: "BufferedIOBase")  [comparison-overlap]
test_other.py:24: error: Incompatible types in assignment (expression has type "BinaryIO", variable has type "RawIOBase")  [assignment]
test_other.py:29: error: Non-overlapping identity check (left operand type: "BinaryIO", right operand type: "RawIOBase")  [comparison-overlap]
test_other.py:31: error: Incompatible types in assignment (expression has type "TextIO", variable has type "TextIOBase")  [assignment]
test_other.py:37: error: Non-overlapping identity check (left operand type: "TextIO", right operand type: "TextIOBase")  [comparison-overlap]
Found 6 errors in 1 file (checked 2 source files)
make: *** [makefile:4: mypy] Error 1

```

- https://github.com/python/typeshed/blob/master/stdlib/io.pyi
- https://github.com/python/typeshed/blob/master/stdlib/builtins.pyi
- https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L345

```
```
